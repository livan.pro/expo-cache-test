import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

const HOST = 'https://placehold.co';

export default function App() {
  return (
    <View style={styles.container}>
      <Button
        title="Fetch"
        onPress={async () => {
          const res = await fetch(`${HOST}/600x400.png`);
          console.log(`Status: ${res.status} ${res.statusText}`);
          console.log(
            'Headers:\n',
            Array.from(res.headers)
              .map(([key, val]) => `${key}: ${val}`)
              .join('\n'),
          );
        }}
      />
      <Text>Open up App.js to start working on your app!</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
